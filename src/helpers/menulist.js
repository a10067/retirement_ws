import Pepperoni from '../assets/pepperoni.jpg';
import Sausage from '../assets/pedrotechspecial.jpg';
import Veggie from '../assets/vegan.jpg';
import Super from '../assets/margherita.jpg';
import Crusty from '../assets/pizza.jpeg';
import Rich from '../assets/Rich.jpg';
import pdf from '../assets/CG_719S_CPT_LIC.pdf'


export const MenuList = [
    {
        name: "Pepperoni Pizza",
        image: pdf,
        price: 12.99, 
    },
    {
        name: "Sausage Pizza",
        image: Sausage,
        price: 13.99,
    },
    {
        name: "Veggie Pizza",
        image: Veggie,
        price: 11.99,
    },
    {
        name: "Super Pizza",
        image: Super,
        price: 99.99,
    },
    {
        name: "Crusty Special",
        image: Crusty,
        price: 55.99,
    },
    {
        name: "Rich Blood",
        image: Rich,
        price: 1289.99,
    },
];